package com.syncfusion;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;

public class Wordcount {

    // Template arguments state the type of input key,value and output key, value
    public static class Map extends MapReduceBase implements Mapper<LongWritable, // the input's key type
            Text, // the value of the input (line of text in this case)
            Text, // the key type of the output. The word that was seen in this case.
            IntWritable> // the value type of the output. The number "1" for each time the word was seen. 
    {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> oc, Reporter rep) throws IOException {
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                word.set(tokenizer.nextToken());
                oc.collect(word, one);
            }
        }
    }

    public static class Reduce extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        @Override
        public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
            int sum = 0;
            while (values.hasNext()) {
                sum += values.next().get();
            }
            output.collect(key, new IntWritable(sum));
        }
    }
    
    public static void main(String[] args) throws IOException {
        
       JobConf jobConf = new JobConf(Wordcount.class);
	jobConf.setJobName("Word Count example");
	jobConf.setOutputKeyClass(Text.class);
	jobConf.setOutputValueClass(IntWritable.class);
	jobConf.setMapperClass(Map.class);
	jobConf.setReducerClass(Reduce.class);
	jobConf.setInputFormat(TextInputFormat.class);
	jobConf.setOutputFormat(TextOutputFormat.class);
	FileInputFormat.addInputPath(jobConf,new Path(args[0]));
	FileOutputFormat.setOutputPath(jobConf,new Path(args[1]));
	JobClient.runJob(jobConf);
    }
}
