﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Hadoop.MapReduce;

namespace WordCountSample
{
    public class WordCountMapper : MapperBase
    {
        public override void Map(string inputLine, MapperContext context)
        {
            try
            {
                string[] words = inputLine.Split(' ');
                foreach (string word in words)
                    context.EmitKeyValue(word, "1");
            }
            catch (ArgumentException ex)
            {
                return;
            }

        }
    }
}
