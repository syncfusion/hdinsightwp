﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Hadoop.MapReduce;

namespace WordCountSample
{
    public class WordCountJob : HadoopJob<WordCountMapper, WordCountReducer>
    {
        public WordCountJob()
        {
        }

        public override HadoopJobConfiguration Configure(ExecutorContext context)
        {
            HadoopJobConfiguration config = new HadoopJobConfiguration();
            config.InputPath = "warpeace";
            config.OutputFolder = "warpeacecountcs";
            return config;
        }
    }
}
