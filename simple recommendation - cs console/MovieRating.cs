﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;

namespace recommend
{
    public class MovieRating
    {
        [CsvField(Index = 0)]
        public string Critic { get; set; }

        [CsvField(Index = 1)]
        public string Movie { get; set; }

        [CsvField(Index = 2)]
        public double Rating { get; set; }

        public static IEnumerable<MovieRating> Read()
        {
            var config = new CsvConfiguration {HasHeaderRecord = false};
            using (StreamReader fileReader = File.OpenText(@"ratings.csv"))
            using (var csvReader = new CsvReader(fileReader, config))
            {
                while (csvReader.Read())
                {
                    var record = csvReader.GetRecord<MovieRating>();
                    yield return record;
                }
            }
        }
    }
}