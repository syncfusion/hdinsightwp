﻿using System;
using System.Linq;

namespace recommend
{
    internal class Program
    {
        public static void Main()
        {
            MovieRating[] movieRating = MovieRating.Read().ToArray();

            Recommendation[] results = Analysis.GetRelatedProducts(movieRating, "Superman Returns");

            foreach (Recommendation result in results)
            {
                Console.WriteLine(result.Movie);
                Console.WriteLine(result.Rating);
            }
        }
    }
}