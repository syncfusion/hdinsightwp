﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqStatistics;

namespace recommend
{
    // Define other methods and classes here
    public class Analysis
    {
        public static Recommendation[] GetRelatedProducts(MovieRating[] movieRatings, string movie, double threshold = -1)
        {
            var allMovies = movieRatings.Select(x => x.Movie).Distinct();

            var results = allMovies.Where(c => c != movie)
                .Select(c => new Recommendation()
                    {Movie = c, Rating = Analysis.CalculateCorrelation(movieRatings, movie, c)})
                .Where(x => x.Rating > threshold)
                .OrderByDescending(x => x.Rating);

            return results.ToArray();
        }

        // calculate the correlation between two movies using ratings by the same critic
        // the stronger the correlation the more similar the two movies can be considered to be
        private static double CalculateCorrelation(MovieRating[] movieRatings, string movie1, string movie2)
        {
            Console.WriteLine(movie1);
            Console.WriteLine(movie2);
            // get the critics who have rated either movie
            var items1 = movieRatings.Where(item => item.Movie == movie1).ToArray();
            var items2 = movieRatings.Where(item => item.Movie == movie2).ToArray();

            // critics who have rated both movies
            var commonItems = items1.Intersect(items2, new PropertyComparer<MovieRating>("Critic")).Select(item => item.Critic);
            
            // no common critics - seen correlation is 0.0
            if (!commonItems.Any())
                return 0.0;

            var ratings1 = new List<double>();
            var ratings2 = new List<double>();

            foreach (var critic in commonItems)
            {
                DumpWithOffset(critic);
                var r1 = items1.Where(i => i.Critic == critic).Select(i => i.Rating).First();
                ratings1.Add(r1);
                DumpWithOffset(r1);
                var r2 = items2.Where(i => i.Critic == critic).Select(i => i.Rating).First();
                ratings2.Add(r2);
                DumpWithOffset(r2);
            }

            return ratings1.Pearson(ratings2);
        }

        private static void DumpWithOffset(object text)
        {
            Console.WriteLine("*" + text.ToString());
        }
    }
}
