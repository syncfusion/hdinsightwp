﻿namespace recommend
{
    public class Recommendation
    {
        public string Movie { get; set; }
        public double Rating { get; set; }
    }
}