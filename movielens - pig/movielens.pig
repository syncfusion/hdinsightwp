-- Load MovieLens file u.data twice since we need to do a self join to obtain unique pairs of movies as before
-- This script assumes that you have uploaded the u.data file and u.item file into a folder named movielens on HDFS
ratings1 = load 'movielens/u.data' as (critic:long, movie:long, rating:double);
ratings2 = load 'movielens/u.data' as (critic:long, movie:long, rating:double);

-- Join by critic name
combined = JOIN ratings1 BY critic, ratings2 BY critic;

-- Since movies are identified by ID of type long we filter and remove cases where both movies are identical
-- The resulting relation contains unique pairs of movies
filtered = FILTER combined BY ratings1::movie != ratings2::movie;

-- project intermediate results
movie_pairs = FOREACH filtered GENERATE 	ratings1::critic AS critic1,
						ratings1::movie AS movie1,
						ratings1::rating AS rating1,
						ratings2::critic AS critic2, 
						ratings2::movie AS movie2,
						ratings2::rating AS rating2;

-- group by movie names
grouped_ratings = group movie_pairs by (movie1, movie2);

-- calculate correlation in rating values
correlations = foreach grouped_ratings generate group.movie1 as movie1,group.movie2 as movie2, 
	FLATTEN(COR(movie_pairs.rating1, movie_pairs.rating2)) as (var1, var2, correlation);

-- Project results removing fields that we do not need
results = foreach correlations generate movie1, movie2, correlation;

-- Load item names and do a join to get actual names instead of just ID refererces
-- Notice that the separator between fields is a '|' in the file u.item.
movies = load 'movielens/u.item' using PigStorage('|') as (movie:long, moviename:chararray);
-- Get the name of the first movie
named_results = JOIN results BY movie1, movies BY movie;
-- Get the name of the second movie
named_results2 = JOIN named_results BY results::movie2, movies BY movie;

-- Write the results to HDFS
-- Please ensure that this folder does not exist
-- remove with hadoop fs -rmr movielensout if it exists
STORE named_results2 INTO 'movielensout';